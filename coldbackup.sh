#!/bin/bash
# Basic script to sync up a local directory with an Azure Blob Storage Container
# You need to set up the corresponding environment variables for authentication and authorization on Azure
# AZURE_BACKUP_STORAGE_SAS: Shared Access Signature
# AZURE_BACKUP_STORAGE_URL: URL to Azure Blob Storage Account
# AZURE_BACKUP_STORAGE_ACCOUNT: (optional) Name given to the storage account
  
# Source global environment variables
source /etc/environment

if [ -z "${1}" ]; then
    echo ERROR: "The path to source directory is missing!";
    echo "Syntax is: coldbackup.sh </path/to/directory> <container/sub/dir/if/any>";
    logger "ERROR (coldbackup.sh): The path to source directory is missing!";
    exit 1;
fi

if [ -z "${2}" ]; then
    echo ERROR: "The Azure container is missing!";
    echo "Syntax is: coldbackup.sh </path/to/directory> <container/sub/dir/if/any/>";
    logger "ERROR (coldbackup.sh): The Azure container is missing!";
    exit 1;
fi
  
if [ -z "${AZURE_BACKUP_STORAGE_SAS}" ]; then
    echo ERROR: "The Shared Access Signature is missing. Check your Environment Variables. Aborting...";
    logger "ERROR (coldbackup.sh): The Shared Access Signature is missing. Check your Environment Variables. Aborting...";
    exit 1;
fi

if [ ! -d ${1} ]; then
    echo ERROR: "The path '${1}' does not exist or is not a directory.";
    echo "AzCopy's sync mode only supports syncing between two directories. Aborting..."; 
    logger "ERROR: The path '${1}' does not exist or is not a directory.";
    exit 1;
fi

# Format: <URL>/<container>?<SAS>
DESTINATION=${AZURE_BACKUP_STORAGE_URL}/${2}${AZURE_BACKUP_STORAGE_SAS};
  
# Arrest the output from AzCopy to be used for messaging
OUTPUT="$(/usr/local/sbin/sysadminscripts/azcopy/azcopy sync ${1} ${DESTINATION} --recursive=true)";

# Let's log to syslog here in case mail() errors out, i.e. is not installed or something else
logger "ColdBackup to Azure: ${OUTPUT}";
 
HOSTNAME="$(hostname -f)";

#Mail out notification
SUBJECT="Backup Operation Report from ${HOSTNAME} to Azure Cold Storage ${AZURE_BACKUP_STORAGE_ACCOUNT}";

mail -s "${SUBJECT}" root <<EOF
Source: ${1}
Container: ${DESTINATION}
  
${OUTPUT}
EOF

if [ "${3}" == '-v' ]; then
    echo "Task: ${SUBJECT}";
    echo "Source: ${1}";
    echo "Container: ${DESTINATION}";
    echo "Report: ${OUTPUT}";
fi

exit;
