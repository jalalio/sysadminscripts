#!/bin/bash
# Basic script to sync up Azure Blob Storage Container to a local directory
# You need to set up the corresponding environment variables for authentication and authorization on Azure
# DOMAD_BACKUP_STORAGE_SAS: Shared Access Signature
# AZURE_BACKUP_STORAGE_URL: URL to Azure Blob Storage Account
# DOMAD_BACKUP_STORAGE_ACCOUNT: (optional) Name given to the storage account

# Source global environment variables
source /etc/environment
DOMAD_URL="https://domad.blob.core.windows.net"
DOMAD_BACKUP_STORAGE_ACCOUNT="domad"

if [ -z "${1}" ]; then
    echo ERROR: "The Azure container is missing!";
    echo "Syntax is: backup_domad_to_local.sh <container/sub/dir/if/any/> </path/to/directory>";
    logger "ERROR (backup_domad_to_local.sh): The Azure container is missing!";
    exit 1;
fi
  
if [ -z "${2}" ]; then
    echo ERROR: "The path to target directory is missing!";
    echo "Syntax is: backup_domad_to_local.sh <container/sub/dir/if/any> </path/to/directory>";
    logger "ERROR (backup_domad_to_local.sh): The path to target directory is missing!";
    exit 1;
fi
  
if [ -z "${DOMAD_BACKUP_STORAGE_SAS}" ]; then
    echo ERROR: "The Shared Access Signature is missing. Check your Environment Variables. Aborting...";
    logger "ERROR (backup_domad_to_local.sh): The Shared Access Signature is missing. Check your Environment Variables. Aborting...";
    exit 1;
fi

if [ ! -d ${2} ]; then
    echo ERROR: "The path '${2}' does not exist or is not a directory.";
    echo "AzCopy's sync mode only supports syncing between two directories. Aborting..."; 
    logger "ERROR (backup_domad_to_local.sh): The path '${2}' does not exist or is not a directory."; 
    exit 1;
fi

# Format: <URL>/<container>?<SAS>
SOURCE_CONTAINER=${DOMAD_URL}/${1}${DOMAD_BACKUP_STORAGE_SAS};
  
# Arrest the output from AzCopy to be used for messaging
OUTPUT="$(/usr/local/sbin/sysadminscripts/azcopy/azcopy sync ${SOURCE_CONTAINER} ${2} --recursive=true)";

# Let's log to syslog here in case mail() errors out, i.e. is not installed or something else
logger "DoMaD Local backup from Coldbackup at Azure: ${OUTPUT}";
 
HOSTNAME="$(hostname -f)";

#Mail out notification
SUBJECT="Backup Operation Report from Azure Cold Storage ${DOMAD_BACKUP_STORAGE_ACCOUNT} to ${HOSTNAME}";

mail -s "${SUBJECT}" root <<EOF
Target: ${2}
Container: ${SOURCE_CONTAINER}
  
${OUTPUT}
EOF

if [ "${3}" == '-v' ]; then
    echo "Task: ${SUBJECT}";
    echo "Container: ${SOURCE_CONTAINER}";
    echo "Target: ${2}";
    echo "Report: ${OUTPUT}";
fi

exit;
