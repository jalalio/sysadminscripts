#!/bin/bash
# Basic script to sync up a local directory with an Azure Blob Storage Container
# You need to set up the corresponding environment variables for authentication and authorization on Azure
# AZURE_BACKUP_STORAGE_SAS: Shared Access Signature
# AZURE_BACKUP_STORAGE_URL: URL to Azure Blob Storage Account
# AZURE_BACKUP_STORAGE_ACCOUNT: Name given to the storage account
    
if [ -z "${1}" ]; then
    echo ERROR: "The container name is missing!";
    echo "Syntax is: make_storage_container.sh <container>";
    exit 1;
fi

if [ -z "${AZURE_BACKUP_STORAGE_SAS}" ]; then
    echo ERROR: "The Shared Access Signature is missing. Check your Environment Variables. Aborting...";
    exit 1;
fi

if [ -z "${AZURE_BACKUP_STORAGE_URL}" ]; then
    echo ERROR: "The backup storage URL is missing. Check your Environment Variables. Aborting...";
    exit 1;
fi

if [ -z "${AZURE_BACKUP_STORAGE_ACCOUNT}" ]; then
    echo ERROR: "The backup storage account is missing. Check your Environment Variables. Aborting...";
    exit 1;
fi

# Format: <URL>/<container>?<SAS>
DESTINATION=${AZURE_BACKUP_STORAGE_URL}/${1}${AZURE_BACKUP_STORAGE_SAS};
  
# Arrest the output from AzCopy to be used for messaging
OUTPUT="$(/usr/local/sbin/sysadminscripts/azcopy/azcopy make ${DESTINATION})";

# Let's log to syslog here in case mail() errors out, i.e. is not installed or something else
logger "Making container at Azure Blob Storage: ${OUTPUT}";
 
